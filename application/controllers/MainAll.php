<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MainAll extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}
    
    public function logout(){
        unset($_SESSION["double_log"]);
        redirect(base_url()."admin/login");
    }
}
