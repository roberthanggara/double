<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainuser extends CI_Controller {

	public function __construct(){
		parent::__construct();	
		
		$this->load->model("user/main_user", "mu");
		$this->load->library("response_message");

		if($this->session->userdata("double_log")["is_log"] != 1){
            redirect(base_url());
        }else{
        	if($this->session->userdata("double_log")["jenis_admin"] != 1){
                redirect(base_url());
            }
        }
        // print_r($this->session->userdata("double_log")["jenis_admin"]);
	}

	public function index(){
		$data["page"] = "home";
		$this->load->view('index', $data);
	}

#========================================================================================================
#----------------------------------------- penjualan ----------------------------------------------------
#========================================================================================================
	public function index_oi(){
		$data["page"] = "penjualan";
		$this->load->view('index', $data);
	}

	public function index_penjualan(){
		$data["page"] = "penjualan";

		$id_admin = $this->session->userdata("double_log")["id_admin"];
		$data["penjualan"] = $this->mu->get_laporan_all_desc($id_admin);

		$this->load->view('index', $data);
	}

	public function indexing_penjualan(){
		$periode = $this->input->post("periode");
		$th = $this->input->post("th");

		// print_r($_POST);

		$this->indexmain_penjualan($periode, $th);
	}

	public function indexmain_penjualan($periode, $th){
		$data["page"] = "penjualan";

		$id_admin = $this->session->userdata("double_log")["id_admin"];

		$data["penjualan"] = $this->mu->get_laporan_where($id_admin, (int)$periode, $th);

		$graph_data = array();
		$key_graph = 0;
		foreach ($data["penjualan"] as $r_data => $v_data) {
			$graph_data[$key_graph]["period"] = explode("-", $v_data->tgl)[2];
			$graph_data[$key_graph]["val_data"] = (int)$v_data->sales_net;
			$key_graph++;
		}
		$data["graph_data"] = json_encode($graph_data);

		$this->load->view('index', $data);
	}

	
	public function validaiton_form(){
		$config_val_input = array(
            array(
                'field'=>'tgl',
                'label'=>'Tanggal Input',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'penjualan',
                'label'=>'Penjualan',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_penjualan(){

		print_r("<pre>");
		if($this->validaiton_form()){
			$tgl = $this->input->post("tgl");
			$penjualan = $this->input->post("penjualan");

			$id_admin = $this->session->userdata("double_log")["id_admin"];
			$tgl_input = date("Y-m-d h:i:s");
			
			$data = array(
						"id_lap"=>"",
						"id_admin"=>$id_admin,
						"tgl_input"=>$tgl_input,
						"tgl"=>$tgl,
						"penjualan"=>$penjualan,
					);

			$insert = $this->mu->insert_laporan($data);
			if($insert){
				$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
				$detail_msg = null;
			}else {
				$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
				$detail_msg = null;
			}

		}else {
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
			$detail_msg = array(
					"tgl" => form_error("tgl"),
					"sales_net" => form_error("sales_net")
				);
		}

		$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
		$this->session->set_flashdata("response_laporan", $msg_array);

		// print_r($msg_array);
		redirect(base_url()."admin/pendapatan");
	}

	public function delete_penjualan($id_penjualan){
		if($this->mu->delete_laporan(array("id_lap"=>$id_penjualan))){
			$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
			$detail_msg = null;
		}else {
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
			$detail_msg = null;
		}
		$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
		print_r($msg_array);
		redirect(base_url()."admin/pendapatan");
	}

	public function index_up_penjualan(){
		$id_lap = $this->input->post("id_lap");
		$data["status"] = false;
		$data["val"] = null;
			
		if(!empty($this->mu->get_laporan_where_index_update($id_lap))){
			$data["status"] = true;
			$data["val"] = $this->mu->get_laporan_where_index_update($id_lap);
		}

		print_r(json_encode($data));
	}

	public function up_penjualan(){
		if($this->validaiton_form()){
			$id_lap = $this->input->post("id_lap");

			$tgl = $this->input->post("tgl");
			$penjualan = $this->input->post("penjualan");

			$id_admin = $this->session->userdata("double_log")["id_admin"];
			$tgl_input = date("Y-m-d h:i:s");

			$data = array(
						"tgl" 		=> $tgl,
						"penjualan" => $penjualan,
						"id_admin"	=> $id_admin,
						"tgl_input" => $tgl_input
					);

			$where = array(
						"id_lap" => $id_lap
					);


			$update = $this->mu->update_laporan($data,$where);
			if($update){
				$main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
				$detail_msg = null;
			}else {
				$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
				$detail_msg = null;
			}

		}else {
			$main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
			$detail_msg = array(
					"tgl" => form_error("tgl"),
					"penjualan" => form_error("penjualan"),
				);
		}

		$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
		$this->session->set_flashdata("response_laporan", $msg_array);

		// print_r($where);
		redirect(base_url()."admin/pendapatan");
	}
#========================================================================================================
#----------------------------------------- penjualan ----------------------------------------------------
#========================================================================================================

#========================================================================================================
#----------------------------------------- report_penjualan ---------------------------------------------
#========================================================================================================
	public function index_report_penjualan(){
		$data["page"] = "report_penjualan";
		$id_admin = $this->session->userdata("double_log")["id_admin"];
		$data_all = $this->mu->get_laporan_all($id_admin);

		if(isset($_POST["tipe_choose"])){
			$tipe_choose 	= $this->input->post("tipe_choose");
			$periode 		= $this->input->post("periode");
			$th 			= $this->input->post("th");

			if($tipe_choose == "1"){
				$data_all = $this->mu->get_laporan_where($id_admin, $periode, $th);	
			}
		}

		

		$data["penjualan"] = $data_all;
		// print_r($data);

		$this->load->view('index', $data);
	}

	public function cetak_report_penjualan(){
		$data["page"] = "report_penjualan";
		$id_admin = $this->session->userdata("double_log")["id_admin"];
		$data_all = $this->mu->get_laporan_all($id_admin);

		if(isset($_POST["tipe_choose"])){
			$tipe_choose 	= $this->input->post("tipe_choose");
			$periode 		= $this->input->post("periode");
			$th 			= $this->input->post("th");

			if($tipe_choose == "1"){
				$data_all = $this->mu->get_laporan_where($id_admin, $periode, $th);	
			}
		}

		

		$data["penjualan"] = $data_all;
		// print_r($data);

		$this->load->view('user/report_penjualan_cetak', $data);
	}
#========================================================================================================
#----------------------------------------- report_penjualan ---------------------------------------------
#========================================================================================================



	public function index_laporan(){
		$data["page"] = "laporan";
		$this->load->view('index',$data);
	}

}
