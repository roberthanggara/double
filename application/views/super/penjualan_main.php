            <?php
            // print_r("<pre>");
                // print_r($penjualan);
            ?>

                <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Penjualan</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <br>
                <!-- Row -->
                <div class="row">

                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">
                            
                <!-- -----------------------------------------------------------------------------------------Data Filter-------------------------------------------------------------------------------------- -->
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Filter Data</h4>
                            </div>
                            <div class="card-body">
                                <form method="post" action="<?= base_url()."super/Mainreportsuper/show_report";?>">
                                        <div class="row">
                                            <div class="col-md-2">
                                                
                                                    &nbsp;
                                                    <label class="control-label">Periode</label>
                                                    
                                                        <select class="form-control" name="periode" id="periode">
                                                            <?php
                                                                $arr_month = array("Januari", "Februari", "Maret" ,"April", "Mei", "Juni", "Juli", "Agustus", "Septermber", "Oktober", "November", "Desember");
                                                                foreach ($arr_month as $r_arr_month => $v_arr_month) {
                                                                    echo "<option value=\"".($r_arr_month+1)."\">".$v_arr_month."</option>";
                                                                }
                                                            ?>
                                                        </select>
                                                    
                                                
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-2">
                                                
                                                    <label class="control-label">Tahun</label>
                                                    
                                                    <input type="number" class="form-control" placeholder="300000" id="th" name="th" value="<?= date("Y");?>" required="">
                                                    
                                                
                                            </div>
                                            <!--/span-->

                                            <div class="col-md-4">
                                               
                                                    <label class="control-label">Toko Cabang</label>
                                                        <select class="form-control" name="cabang" id="cabang">
                                                            <?php
                                                            if(!empty($toko)){
                                                                foreach ($toko as $r_toko => $v_toko) {
                                                                    echo "<option value=\"".$v_toko->id_toko."\">".$v_toko->cabang." - ".$v_toko->alamat."</option>";
                                                                }
                                                            }
                                                                
                                                            ?>
                                                        </select>
                                                    
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <div class="col-md-2">
                                                        <label class="control-label">&nbsp;</label>
                                                        <br><button type="button" class="btn btn-success" id="filter" name="filter" style="vertical-align: bottom;">Filter</button>
                                                        <!-- <button type="submit" class="btn btn-success"  style="vertical-align: bottom;">Filter</button> -->
                                                    </div>&nbsp;&nbsp;&nbsp;
                                                    <div class="col-md-2">
                                                        <label class="control-label">&nbsp;</label>
                                                        
                                                        <button type="submit" class="btn btn-info" style="vertical-align: bottom;">Cetak</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                             
                                        </div>
                                </form>
                            </div><br><br>
                <!-- -----------------------------------------------------------------------------------------Data Table-------------------------------------------------------------------------------------- -->
                            

                        </div>
                    </div>
                </div>
                <!-- Row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Grafik Data Sales Net</h4>
                                <ul class="list-inline text-center m-t-40">
                                    <li>
                                        <h5><i class="fa fa-circle m-r-5 text-info"></i>Sales Net</h5>
                                    </li>
                                </ul>
                                <div id="extra-area-chart"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Data Table</h4>
                                    
                                    <div class="table-responsive m-t-40">
                                        <table id="myTable" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Position</th>
                                                    <th>Office</th>
                                                    <th>Age</th>
                                                    <th>Start date</th>
                                                    <th>Salary</th>
                                                </tr>
                                            </thead>
                                            <tbody id="output_laporan">

                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

           <!--Morris JavaScript -->
            <script src="<?= base_url();?>template_admin/assets/plugins/raphael/raphael-min.js"></script>
            <script src="<?= base_url();?>template_admin/assets/plugins/morrisjs/morris.js"></script>
                                

            <script type="text/javascript">
                
                $(document).ready(function(){



                });

                function set_chart(val_data){
                    $(function () {
                    "use strict";
                    // Extra chart
                     Morris.Area({
                            element: 'extra-area-chart',
                            data: val_data,
                                    lineColors: ['#009efb'],
                                    xkey: 'period',
                                    ykeys: ['val_data'],
                                    labels: ['Sales Net'],
                                    pointSize: 0,
                                    lineWidth: 0,
                                    resize:false,
                                    fillOpacity: 0.8,
                                    behaveLikeLine: true,
                                    gridLineColor: '#e0e0e0',
                                    hideHover: 'auto'
                            
                        });
                    }); 
                }

                function currency(x){
                    return x.toLocaleString('us-EG');
                }

                $("#filter").click(function(){
                                        var data_main =  new FormData();
                                        data_main.append('periode', $("#periode").val());
                                        data_main.append('th', $("#th").val());
                                        data_main.append('cabang', $("#cabang").val());

                                        $.ajax({
                                            url: "<?php echo base_url()."/super/Mainreportsuper/show_tbl/";?>", // point to server-side PHP script 
                                            dataType: 'html',  // what to expect back from the PHP script, if anything
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            data: data_main,                         
                                            type: 'post',
                                            success: function(res){
                                                // console.log(res);
                                                // res_update(res);
                                                // $("#myTable").html(res);
                                                show_search(res);
                                            }
                                        });
                });            

                function show_search(res){
                    
                    var data = JSON.parse(res).penjualan;
                    // console.log(data);
                    if(data.length != 0){
                        // console.log(data);
                        var str_tbl = "";
                        var graph = [];
                        for (var i = 0; i<data.length ; i++) {
                                // console.log(data[0].id_lap);
                                str_tbl += "<tr><td>"+data[i].tgl+"</td>"+
                                            "<td>"+data[i].sales_net+"</td>"+
                                            "<td>"+data[i].struk+"</td>"+
                                            "<td>"+data[i].pergantian+"</td>"+
                                            "<td>"+data[i].variance+"</td>"+
                                            "<td>"+data[i].discount+"</td>"+
                                            "<td>"+data[i].dsi_ini+"</td></tr>";
                                val_graph = {
                                        period: data[i].tgl.split("-")[2],
                                        val_data: data[i].sales_net,
                                    };
                                graph.push(val_graph);
                                
                        }
                        console.log(graph);
                        set_chart(graph);
                        $("#output_laporan").html(str_tbl);
                    }
                    
                }
                
            </script>