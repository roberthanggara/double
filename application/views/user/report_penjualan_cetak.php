                <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Single Exponensial Forecasting</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    
                </div>
                
            </div> -->
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">            
                <br>
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-inverse">                
                            <!-------------------------------------------Data Table------------------------------------>
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Data Pendapatan</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example24" border="1" width="30%" style="border-collapse: collapse;">
                                        <thead>
                                            <tr>
                                                <th width="10%">Periode</th>
                                                <th width="15%">Tgl.</th>
                                                <th width="15%">Penjualan Real</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Periode</th>
                                                <th>Tgl.</th>
                                                <th>Penjualan Real</th>
                                            </tr>
                                        </tfoot>
                                        <tbody id="output_laporan">
                                            <?php
                                                if(!empty($penjualan)){
                                                    foreach ($penjualan as $r_penjualan => $v_penjualan){
                                                        echo "<tr>
                                                                <td align=\"center\">".($r_penjualan+1)."</td>
                                                                <td align=\"center\">".$v_penjualan->tgl."</td>
                                                                <td align=\"right\">Rp. ".number_format($v_penjualan->penjualan, 2,'.', ',')."</td>
                                                                
                                                            </tr>";
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- Row -->
            </div>

            